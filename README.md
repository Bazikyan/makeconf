# makeconf

Bash script to generate nginx config files

## Installation
    mkdir -p ~/scripts
    cd ~/scripts
    wget -O makeconf.tar.gz https://gitlab.com/Bazikyan/makeconf/-/archive/main/makeconf-main.tar.gz
    tar -xf makeconf.tar.gz
    rm makeconf.tar.gz
    cd makeconf-main
    chmod 751 makeconf.sh
    sudo ln -s ~/scripts/makeconf-main/makeconf.sh  /usr/local/bin/makeconf

## Usage
To create config file run

    sudo makeconf domain

Replace domain with your domain name

If file already exists you can add force option to replace it

    sudo makeconf domain -f
