#!/bin/bash

SOURCE_DIR="$(dirname $(readlink -f "$0"))"
. $SOURCE_DIR/config

DOMAIN=$1

if [ -z "$DOMAIN" ];
then
 echo "Domain name missing"
 exit 1;
fi


if [ "$2" == "-f" ];
then
 IS_FORCE=true
else
 IS_FORCE=false
fi

mkdir -p $DIR
FILENAME=$DIR/$DOMAIN.conf

if [ -f $FILENAME -a "$IS_FORCE" = false ];
then
 echo "File already exists"
 echo "Add -f option to replace it"
else
 sed -e "s;%DOMAIN%;$DOMAIN;g" $SOURCE_DIR/template.txt > $FILENAME
 echo "File successfully created"
 echo "$FILENAME"
fi
